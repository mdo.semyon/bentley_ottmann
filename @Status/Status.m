classdef Status<handle
    
%Preparata. �������������� ���������.
% �������, ��� ��������� T ������ ������ ������������, ������� ���������� �� ���� ����, 
% ��� ��������� �������� ����� �������. ������� ������ � ��� ������������ � �������� ���, 
% �� ��� ������ �������� ������. ������������ ����� ���������� ������ � ���� �������: 
%
% 1. ���������� ����� ����� ������� s. � ���� ������ s ���� �������� � ��������� ������. 
% 2. ���������� ������ ����� ������� s. � ���� ������ s ���� ������� �� ��������� ������, 
%    ��������� �� ������ �� ������� � ����������� ���������. 
% 3. ���������� ����� ����������� �������� s1 � s2. ����� s1 � s2 �������� ������� � ��������� ������. 
% 
% � ��������� ������ T, ����������� ������ ���������� ������, ������ ���� ������������� ��������� 
% ��������: 
% 
% a. ��������(s,T). �������� ������� s � ������ ������������, �������������� � T. 
% b. �������(s,T). ������� ������� s �� T. 
% c. ���(s,T). ����� ��� �������, �������������� ��������������� ��� s � T. 
% d. ���(s,T). ����� ��� �������, �������������� ��������������� ��� s � T. 

    
    properties
        NiL
        root
    end
    
    methods
        function rbTree=Status(s)
            import Nodes.*
            rbTree.root=ColoredStatusNode(s);

            % root node is always black!!!
            rbTree.root.color=Colors.black;

            % NiL node is always black!!!
            rbTree.NiL=ColoredStatusNode(NaN);
            rbTree.NiL.color=Colors.black;

            rbTree.NiL.left=rbTree.root;
            rbTree.NiL.right=rbTree.root;
            rbTree.root.parent=rbTree.NiL;
            rbTree.root.left=rbTree.NiL;
            rbTree.root.right=rbTree.NiL;
        end
        
        %a. ��������(s,T)
        function node = addSegment(rbTree,s)
            import Nodes.*
            
            node=ColoredStatusNode(s);
            rbTree.addNode(node);
        end
        
        function addNode(rbTree,node)
            import Nodes.*
            
            % 1. ���������� ����� ����� ������� s. � ���� ������ s ���� �������� � ��������� ������. 
            % ������� �������� ������� ���������� x ����� ���������� ������
            % ������������ ������� s
            current_x = node.s.p.x;
            
            y=rbTree.NiL;
            x=rbTree.root;
            
            while x~=rbTree.NiL
               y=x;
               if node.s.get_y(current_x) < x.s.get_y(current_x)
                   x=x.left;
               elseif node.s.get_y(current_x) > x.s.get_y(current_x)
                   x=x.right;
               else
                   %��������� ������ ����
                   x = x.right;
                   %return 
                   %{
                   if node.s.p.y < x.s.get_y(current_x)
                       x = x.left;
                   elseif node.s.p.y > x.s.get_y(current_x)
                       x = x.right;
                   else
                       return
                   end
                   %}
               end
            end
            
            node.parent=y;
            if y==rbTree.NiL
                rbTree.root=node;
            else
                if node.s.get_y(current_x) < y.s.get_y(current_x)
                    y.left=node;
                elseif node.s.get_y(current_x) > y.s.get_y(current_x)
                    y.right=node;
                
                else
                    %��������� ������ ����
                    y.right = node;
                    %return
                    %{
                    if node.s.p.y < y.s.get_y(current_x)
                       y.left = node;
                   elseif node.s.p.y > y.s.get_y(current_x)
                       y.right = node;
                   else
                       return
                   end
                    %}
                end
            end
            
            %insert node into tree
            node.left=rbTree.NiL;
            node.right=rbTree.NiL;
            node.color=Colors.red;
            
            %call the fixup method for setting the tree right
            rbInsertFixup(rbTree,node);
        end
        
        function y=rbDeleteNode(rbTree,node)
            import Nodes.*
            if node.left==rbTree.NiL || node.right==rbTree.NiL
                y=node;
            else
                y=treeSuccessor(rbTree,node);
                
            end
            
            if y.left==rbTree.NiL
                x=y.left;
            else
                x=y.right;
            end
            
            x.parent=y.parent;
            
            if y.parent==rbTree.NiL
                rbTree.root=x;
            else
                if y==y.parent.left
                    y.parent.left=x;
                else
                    y.parent.right=x;
                end
            end
            
            if y==node
                node.s.x1=y.s.x1;
            end
            
            if y.color==Colors.black
                rbDeleteFixup(rbTree,x);
            end
            
                    
        end

        % b. �������(s,T), �������� �������� ���� �� �������
        function rbDeleteNode2(rbTree,z)
            import Nodes.*
            
            y = z;
            y_original_color = y.color;
            if z.left==rbTree.NiL
                x=z.right;
                rbTransplant(rbTree,z,z.right);
            elseif z.right == rbTree.NiL
                x = z.left;
                rbTransplant(rbTree,z,z.left);
            else
                y = treeMinimum(rbTree,z.right);
                y_original_color = y.color;
                x = y.right;
                
                if y.parent == z
                    x.parent = y;
                else
                    rbTransplant(rbTree,y,y.right);
                    y.right = z.right;
                    y.right.parent = y;
                end
                %--------
                rbTransplant(rbTree,z,y);
                y.left = z.left;
                y.left.parent = y;
                y.color = z.color;
                %--------
            end
            
            if y_original_color == Colors.black
               rbDeleteFixup(rbTree,x); 
            end
        end
        
        % method for searching for k- key in the tree.
        %���� ��� ����� �� ������������ ����� � ������, �� ������ ���������
        %�������� S' <_x S. ��������� ��� ������� �� ��� �����, � �������
        %�� ��������� ��������� => � ��� �������� ������ ����� �������� ��
        %x.
        %������� ���� ������ ������, ����� x �������� ������������
        %��������.
        %�������� � �������� swap. � ��� ���� ��������� S1 <_x S2 <_x S3
        %... <_x Sn. � ������ ����� ����� x �������� ������ �����������
        %��������, �������� swap �������� ������������ ��������� S1 <_x ... 
        % <_x Sn ������.
     
        function node=treeSearch(rbTree,node,s,x)
            dx = 0.0005;
            
            if ~isa(s,'edge')
                disp('Status::treeSearch parameter s has invalid input type.');
                node = NaN;
                return
            end
            
            while node ~= rbTree.NiL && s.id ~= node.s.id
                 y_1 = single(s.get_y(x));
                 y_2 = single(node.s.get_y(x));

                 
                if y_1 < y_2
                    node=node.left;
                elseif y_1 == y_2
                    %����� ���� � ��������, ����� ��� ������/�����
                    %��������
                    %   \    \         /    /        \   /
                    %    \    \       /    /          \ /
                    %     o    o     o    o      o     o
                    %    /      \   /      \    / \
                    %   /        \ /        \  /   \

                    if isa(node.right.s,'edge') && node.right.s.id == s.id
                        %��������� ����� ��� ��� ������ ���� ��������
                        %node.right.s.id == s.id, �.� �� id ��������
                        node = node.right;
                        return
                    elseif isa(node.left.s,'edge') && node.left.s.id == s.id
                        %��������� ����� ��� ��� ������ ���� ��������
                        %node.right.s.id == s.id, �.� �� id ��������                        
                        node = node.left;
                        return                        
                    else
                        %x ����� �����������, ������� ���� ���������� ���
                        %���� ������� ������
                        if single(s.get_y(x - dx)) < single(node.s.get_y(x - dx))
                            %���� ����� ����, �.�. ��� �� ���� swap
                            node = node.left;
                        else
                            node = node.right;
                        end
                    end
                else
                    node=node.right;
                end
                
                if ~isa(node.s,'edge')
                    node = NaN;
                    break                    
                end
            end
        end
    
        function plot(rbTree,h)
                        
            %f=figure;
            %axes('Parent',f,'XLim',[0 1],'YLim',[0 1],'XTick',[],'YTick',[]);
            %axes(h);
            %grid on;
            ploting(rbTree,h,rbTree.root,0,1,0.9);
        end
        
        function num=maxInTree(rbTree,node)
            while node.right~=rbTree.NiL
                node=node.right;
            end
            num=node.s.p.x;
        end
        function num=minInTree(rbTree,node)
            while node.left~=rbTree.NiL
                node=node.left;
            end
            num=node.s.p.x;
        end
        
        % minimum in tree helper function
        function min=treeMinimum(rbTree,node)
            while node.left~=rbTree.NiL
                node=node.left;
            end
            min=node;
        end        
        
        function num=numberOfNodes(rbTree,node)
            num=countNodes(rbTree,node,0);
        end
        
        % c. ���(s,T)
        function u = Under(rbTree,node)
            
%             if node == rbTree.root
%                 u = NaN;
%                 return
%             end
            
           if node.right ~= rbTree.NiL
               x = node;
               l = 0;               
               
               
               node = node.right;
               %���� ������� �������� �����, ���������� �� ������ ��������
               %���� ���� �� ������ ������� �����
               %  node
               %    \
               %     \
               %     ...
               %       \
               %        o
               %       /  
               while node.left == rbTree.NiL && node.right ~= rbTree.NiL
                   node = node.right;
               end
               
               %  node
               %    \
               %     \
               %     ...
               %       \
               %        o
               %       /
               %     ...
               %     /
               %    o
               if node.left ~= rbTree.NiL
                   while node.left ~= rbTree.NiL
                       node = node.left;
                       l = l + 1;
                   end
               end
               

               %  node
               %    \
               %     o
               if l == 0 %��� � �� ����� �������� �����, ������ ���� ���������� ������ ���� ������� � �����
                    u = x.right;
                    return;
               end
               
               u = node;
           else
               %   o
               %    \
               %     \
               %     ...
               %       \
               %       node
                while isa(node.parent.s,'edge') && node.parent.right == node
                    node = node.parent;
                end
               
                if node == rbTree.root
                    u = NaN;
                    return
                end
               %           o  
               %          /
               %         / 
               %        /
               %       /
               %      /
               %     /
               %    /
               %   o
               %    \
               %     \
               %     ...
               %       \
               %       node                
                if node.parent.left == node
                    u = node.parent;
                    return
                end
                
                u = NaN;
           end
        end
        
        % d. ���(s,T)
        function b = Below(rbTree,node)
%             if node == rbTree.root
%                 b = NaN;
%                 return
%             end
            
           if node.left ~= rbTree.NiL
               x = node;
               r = 0;
               node = node.left;
               
               while node.right == rbTree.NiL && node.left ~= rbTree.NiL
                   node = node.left;
               end
               
               if node.right ~= rbTree.NiL
                   while node.right ~= rbTree.NiL
                       node = node.right;
                       r = r + 1;
                   end               
               end
               
               if r == 0
                  b = x.left;
                  return
               end
               
               b = node;
           else
               while isa(node.parent.s,'edge') && node.parent.left == node
                   node = node.parent;
               end
               
                if node == rbTree.root
                    b = NaN;
                    return
                end
                
                if node.parent.right == node
                    b = node.parent;
                    return
                end
                b = NaN;
           end
        end
    end
    
    methods(Static)
        % 3. ���������� ����� ����������� �������� s1 � s2. ����� s1 � s2 �������� ������� � ��������� ������. 
        function swap(u,v)
            %���� ��� �������� �� �����, ������ ������ �������� �
            %��������������� �����!!!
            
            %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            %�� ����� ������ ��� ��� ��� ��������, ���������� �����.......
            tmp = v.s;
            v.s = u.s;
            u.s = tmp;
            
            v.data = v.s.id;            
            u.data = u.s.id;
        end        
    end
    methods (Access=protected)
        
        % insertaion helper function
        function rbInsertFixup(rbTree,node)
            import Nodes.*
            while node.parent.color==Colors.red
                if node.parent==node.parent.parent.left
                    y=node.parent.parent.right;
                    if y.color==Colors.red
                        node.parent.color=Colors.black;
                        y.color=Colors.black;
                        node.parent.parent.color=Colors.red;
                        node=node.parent.parent;
                    else
                        if node==node.parent.right
                            node=node.parent;
                            leftRotate(rbTree,node);
                        end
                        node.parent.color=Colors.black;
                        node.parent.parent.color=Colors.red;
                        rightRotate(rbTree,node.parent.parent);
                    end
                else
                    y=node.parent.parent.left;
                    if y.color==Colors.red
                        node.parent.color=Colors.black;
                        y.color=Colors.black;
                        node.parent.parent.color=Colors.red;
                        node=node.parent.parent;
                    else
                        if node==node.parent.left
                            node=node.parent;
                            rightRotate(rbTree,node);
                        end
                        node.parent.color=Colors.black;
                        node.parent.parent.color=Colors.red;
                        leftRotate(rbTree,node.parent.parent);
                    end
                            
                end
            end
            rbTree.root.color=Colors.black;
        end
        
        % rotate left helper fuction
        function leftRotate(rbTree,node)
            y=node.right;
            node.right=y.left;
            if y.left~=rbTree.NiL
                y.left.parent=node;
            end
            y.parent=node.parent;
            if node.parent==rbTree.NiL
                rbTree.root=y;
            else
                if node==node.parent.left
                    node.parent.left=y;
                else
                    node.parent.right=y;
                end
                    
            end
            y.left=node;
            node.parent=y;
        end
        
        % rotate right helper function
        function rightRotate(rbTree,node)
            y=node.left;
            node.left=y.right;
            if y.right~=rbTree.NiL
                y.right.parent=node;
            end
            y.parent=node.parent;
            if node.parent==rbTree.NiL
                rbTree.root=y;
            else
                if node==node.parent.right
                    node.parent.right=y;
                else
                    node.parent.left=y;
                end
                    
            end
            y.right=node;
            node.parent=y;
        end
        
        % deletion helper function
        function rbDeleteFixup(rbTree,node)
            import Nodes.*
            while node~=rbTree.root && node.color==Colors.black
                if node==node.parent.left
                    w=node.parent.right;
                    if w.color==Colors.red
                        w.color=Colors.black;
                        node.parent.color=Colors.red;
                        leftRotate(rbTree,node.parent);
                        w=node.parent.right;
                    end
                    
                    if w.left.color==Colors.black && w.right.color==Colors.black
                        w.color=Colors.red;
                        node=node.parent;
                    else
                        if w.right.color==Colors.black
                            w.left.color=Colors.black;
                            w.color=Colors.red;
                            rightRotate(rbTree,w);
                            w=node.parent.right;
                        end
                        w.color=node.parent.color;
                        node.parent.color=Colors.black;
                        w.right.color=Colors.black;
                        leftRotate(rbTree,node.parent);
                        node=rbTree.root;
                    end
                else
                    w=node.parent.left;
                    if w.color==Colors.red
                        w.color=Colors.black;
                        node.parent.color=Colors.red;
                        rightRotate(rbTree,node.parent);
                        w=node.parent.left;
                    end
                    
                    if w.right.color==Colors.black && w.left.color==Colors.black
                        w.color=Colors.red;
                        node=node.parent;
                    else
                        if w.left.color==Colors.black
                            w.right.color=Colors.black;
                            w.color=Colors.red;
                            leftRotate(rbTree,w);
                            w=node.parent.left;
                        end
                        w.color=node.parent.color;
                        node.parent.color=Colors.black;
                        w.left.color=Colors.black;
                        rightRotate(rbTree,node.parent);
                        node=rbTree.root;
                    end
                end
            end
            node.color=Colors.black;
        end
        % successor find helper method
        function successor=treeSuccessor(rbTree,node)
            if node.right~=rbTree.NiL
                successor=treeMinimum(rbTree,node.right);
                return
            end
            y=node.parent;
            while y~=rbTree.NiL && node==y.right
                node=y;
                y=y.parent;
            end
            successor=y;
        end
        
        %�������� ��������������(������) ���� �� �������
        function rbTransplant(rbTree,u,v)
            if u.parent == rbTree.NiL
                rbTree.root = v;
            elseif u == u.parent.left
                u.parent.left = v;
            else
                u.parent.right = v;
            end
            
            v.parent = u.parent;            
        end
        
        function ploting(rbTree,h,node,x1,x2,y)
            import Nodes.*
            if ~isempty(node)&& node~=rbTree.NiL
               x=mean([x1 x2]);
               ploting(rbTree,h,node.left,x1,x,y-0.1);
               ploting(rbTree,h,node.right,x,x2,y-0.1);
               
               if node.color==Colors.red
                   color=[1 0 0];
               else
                   color=[0 0 0];
               end
               patch([x-0.015,x+0.045,x+0.045,x-0.015],[y+0.015,y+0.015,y-0.015,y-0.015],...
                   ones(1,4),'FaceColor',color,'Parent',h);
               
               %text(x-0.005,y,num2str(node.s.p.x),'FontWeight','bold','Color',[1 1 1],'Parent',h);
               text(x-0.005,y,[num2str(node.s.id) ' : ' num2str(node.s.p.x)],'FontSize',6,...
                   'FontWeight','normal','Color',[1 1 1],'Parent',h);
               
               
               try
                   if node==node.parent.left
                       l=line([x x2],[y y+0.1],'Color',[1 0 0],'Parent',h);
                   end
                   if node==node.parent.right
                       l=line([x x1],[y y+0.1],'Color',[0 0 1],'Parent',h);
                   end
                   uistack(l,'bottom') 
               catch
                   return
               end
           end
        end
       
        function numOfNodes=countNodes(rbTree,node,numOfNodes)
             if node~=rbTree.NiL
               numOfNodes=numOfNodes+1;
               numOfNodes=countNodes(rbTree,node.left,numOfNodes);
               numOfNodes=countNodes(rbTree,node.right,numOfNodes);
             end
        end
    end
    
    % get/set methods
    methods
        % get root method
        function root=get.root(rbTree)
           root=rbTree.root; 
        end
        % set root method
        function set.root(rbTree,root)
            rbTree.root=root;
        end
        %getNiL method
        function nil=get.NiL(rbTree)
            nil=rbTree.NiL;
        end
        function set.NiL(rbTree,nil)
            rbTree.NiL=nil;
        end
    end
    
end

