function saveErrorInfoForFurtherDebugging(errorID, name, event, s, status)

    %disp([num2str(errorID),' ',name,' : unable to find status node.']);

    switch name
        case 'treeSearch' 
            disp([num2str(errorID),' ',name,' : unable to find status node.']);
            debug.errorID = errorID;
            debug.name = name;
            debug.point = event.p;
            debug.eventType = event.type;
            debug.segment = s;
            debug.status = status;
            save(['C:\Users\semen.safonov\Desktop\SVNMaps\Lib\Debugging\data\debug_info_',num2str(errorID),'_search.mat'],'debug');
        case 'Under'
            %{
            debug.errorID = errorID;
            debug.name = name;
            debug.point = event.p;
            debug.eventType = event.type;
            debug.status_node = s;
            debug.status = status;
            save(['C:\Users\semen.safonov\Desktop\SVNMaps\Lib\Debugging\data\debug_info_',num2str(errorID),'_under.mat'],'debug');
            %}
        case 'Below'
            %{
            debug.errorID = errorID;
            debug.name = name;
            debug.point = event.p;
            debug.eventType = event.type;
            debug.status_node = s;
            debug.status = status;
            save(['C:\Users\semen.safonov\Desktop\SVNMaps\Lib\Debugging\data\debug_info_',num2str(errorID),'_below.mat'],'debug');
            %}
        otherwise
    end

end
