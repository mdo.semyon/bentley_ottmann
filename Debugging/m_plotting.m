function m_plotting(rbTree,h,node,x1,x2,y)

import Nodes.*
    if ~isempty(node)&& node~=rbTree.NiL
       x=mean([x1 x2]);
       m_plotting(rbTree,h,node.left,x1,x,y-0.1);
       m_plotting(rbTree,h,node.right,x,x2,y-0.1);

       if node.color==Colors.red
           color=[1 0 0];
       else
           color=[0 0 0];
       end
       patch([x-0.015,x+0.045,x+0.045,x-0.015],[y+0.015,y+0.015,y-0.015,y-0.015],...
           ones(1,4),'FaceColor',color,'Parent',h);

       %text(x-0.005,y,num2str(node.s.p.x),'FontWeight','bold','Color',[1 1 1],'Parent',h);
       text(x-0.005,y,[num2str(node.s.id) ' : ' num2str(node.s.p.x)],'FontSize',6,...
           'FontWeight','normal','Color',[1 1 1],'Parent',h);


       try
           if node==node.parent.left
               l=line([x x2],[y y+0.1],'Color',[1 0 0],'Parent',h);
           end
           if node==node.parent.right
               l=line([x x1],[y y+0.1],'Color',[0 0 1],'Parent',h);
           end
           uistack(l,'bottom') 
       catch
           return
       end
   end


end

