classdef ColoredEventNode < Nodes.ColoredNode
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        p
        type
        s1
        s2
    end
    
    methods
        function node=ColoredEventNode(p,t)
            if isa(p,'point')
                data = p.x;
            else
                data = NaN;
            end
            node=node@Nodes.ColoredNode(data);
            node.p = p;
            node.type = t;
        end
        
        % set and get methods for color property.
        function set.p(node,p)
            node.p=p;
        end
        
        function p=get.p(node)
            p=node.p;
        end        

        %type of event:start(1),end(-1),cross(0)
        function set.type(node,t)
            node.type=t;
        end
        
        function t=get.type(node)
            t=node.type;
        end        
        
        %�� ������ ������� ������ � �������� ��� ���� ����� �� �����������
        %������ ������� �� �����, �� ��������� ���� ��������� �������
        function set.s1(node,newS1)
            node.s1=newS1;
        end
        
        function s=get.s1(node)
            s=node.s1;
        end
        
        %��� ����� ��������������� �����������, �� ������ ������� 2 �������
        %��������������� ������ �����
        function set.s2(node,newS2)
            node.s2=newS2;
        end
        
        function s=get.s2(node)
            s=node.s2;
        end       
    end
end

