classdef ColoredStatusNode < Nodes.ColoredNode
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        s
    end
   
    
    methods
        function node=ColoredStatusNode(newS)
            
            if isa(newS,'segment')
                data = newS.id;
            else
                data = NaN; 
            end
            node=node@Nodes.ColoredNode(data);
            node.s = newS;
        end
        
        % set and get methods for color property.
        function set.s(node,newS)
            node.s=newS;
        end
        
        function s=get.s(node)
            s=node.s;
        end        
    end
    
end

