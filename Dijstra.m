	clear all

INF = 10000000000000;

numberOfVertices = 50;

%������ n ...
V = cell(numberOfVertices,1);
j = 0;
for i = 1:numberOfVertices
    p = point(100*randn,100*randn); 
    v = vertex(i,p);
    %������������� ������� ���������
    %�������� ������� � ������   
    V{i} = v;
   
    if rem(i,2) == 0
        j = j + 1;
    end
end

numberOfEdges = j;

E = cell(numberOfEdges,1);
for i = 1:numberOfEdges
    v1 = V{2*i-1};
    v2 = V{2*i};
    e = edge(i,v1,v2);    
    E{i} = e;
end


sE = BentleyOttmann(E,V);

%%splitting edges
sE = sortrows(sE,3);
test = false(numberOfEdges,1);
for i = 1:length(sE)
    
    id1 = sE(i,1);
    id2 = sE(i,2);
    x = sE(i,3);
    y = sE(i,4);
     
    p = point(x,y);
    numberOfVertices = numberOfVertices + 1;
    v = vertex(numberOfVertices,p);
    V{numberOfVertices} = v;
    
    a1 = E{id1};
    numberOfEdges = numberOfEdges + 1;
    a2 = a1.split(numberOfEdges,v);
    E{numberOfEdges} = a2;
    
    b1 = E{id2};
    numberOfEdges = numberOfEdges + 1;
    b2 = b1.split(numberOfEdges,v);
    E{numberOfEdges} = b2;
    
end


% load('E.mat');
% load('V.mat');
% [numberOfVertices,~] = size(V);
% [numberOfEdges,~] = size(E);

fig = figure;
h = axes;
hold on
for i = 1:numberOfEdges
   e = E{i};
   e.plot(h);
end

G = cell(numberOfVertices,1);

%% ������ ����
for i = 1:numberOfVertices
    %�������� ������ ����� ������� � ������ ��������
    test = false(numberOfEdges,1);
    for j = 1:numberOfEdges
        if i == E{j}.v1.id || i == E{j}.v2.id
            test(j)=true;
        end
    end
    edges = E(test,:);
    
    if isempty(edges)
        edges = [];
    end
    
    %������ doubly connected edge list
    G{i} = edges;
end
%load('G.mat');

%% �������� ��������

d = zeros(numberOfVertices,1); 
d(:) = INF;
p = zeros(numberOfVertices,1);
u = zeros(numberOfVertices,1);

% s ��������� �������
s = 3;
d(s) = 0;

for i = 1:numberOfVertices
    v = -1;
    for j= 1:numberOfVertices
        
        %���� ������� ������� j �� ���� ��� ��������, �� �� � ����������� � v � ����� �� ���� �������: 
        %   ���� v �� ��� ��� �������� ������ �������� (-1), 
        %   ���� v �������� ������� � ������� �����������, ��� ������� j. 
        %����� �������, ����� ����� � ������� v �������� �������, ������� �� ���� ��� �������� (�.�. u[v] = false), 
        %� ����� ���� ����� - � ����������� ����������� d.
        if u(j) == 0 && (v == -1 || d(j) < d(v)) 
            v = j;
        end
    end
    if d(v) == INF
        break;
    end

    u(v) = 1;

    %������� ����� ��� ������� v
    edges = G{v};
    %��������� ���-�� ������� ����� ��� ������ �������
    [numberOfAdjacentEdges,~] = size(edges);
    
    for j = 1:numberOfAdjacentEdges

        to = edges{j}.v2.id;
        if to == v
            to = edges{j}.v1.id;
        end
        len = single(edges{j}.get_d());

        if d(v) + len < d(to)

            d(to) = d(v) + len;
            p(to) = v;

        end
    end
end



target = -1;
src = 20;

while target ~= 3
   target = single(p(src));
    
   if target == 0
       break
   end
   
   x1 = V{src}.p.x; 
   x2 = V{target}.p.x;
   y1 = V{src}.p.y;
   y2 = V{target}.p.y;

   plot(h,[x1 x2],[y1 y2],'b');
   
   
   src = target;
end



