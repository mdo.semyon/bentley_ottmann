classdef vertex < handle
    
    properties
        id
        p
    end
    
    methods
        function v=vertex(id,p)
            v.id = id;
            v.p = p;
        end        
    
        function plot(v,h)
            x = v.p.x;
            y = v.p.y;
            
            patch([x - 0.2,x + 0.2,x + 0.2,x - 0.2],...
                  [y + 0.2,y + 0.2,y - 0.2,y - 0.2],...
                  ones(1,4),'FaceColor','b','Parent',h);
            
            %{   
            text(x,y,num2str(v.id),'FontSize',10,...
                   'FontWeight','normal','Color',[0 0 0],'Parent',h);
            %}
           end
    end            

    methods
        %����� - p
        function set.p(v,p)
            v.p=p;
        end
        
        function p=get.p(v)
            p=v.p;
        end
    end
end

