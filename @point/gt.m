function res = gt(obj, val)
    res = obj.x > val.x || abs(obj.x - val.x) == 0 && obj.y > val.y;
end