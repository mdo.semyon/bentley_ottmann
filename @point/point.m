classdef point
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        x
        y
    end
    
    methods
        function the_point=point(x,y)
            the_point.x = x;
            the_point.y = y;
        end        
    end
    
    methods(Static)
        function p = max(a,b)
            if isa(a,'point') && isa(b,'point')
                error('point:max input parameters are not of point type.');
            end
                
            if a > b
                p = a;
            else
                p = b;
            end
        end
        
        function p = min(a,b)
            if isa(a,'point') && isa(b,'point')
                error('point:min input parameters are not of point type.');
            end
            
            if a < b
                p = a;
            else
                p = b;
            end
        end
    end
end

