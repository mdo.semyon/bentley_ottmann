function res = eq(obj, val)
    res = obj.x == val.x && obj.y == val.y;
end