function plotdata(h,Y,mode,N)

    if strcmp(mode,'edges')
%         if ~isa(Y,'edge')
%             disp('plotdata: input format is not edges') 
%             return
%         end
        hold(h,'on');
        
        %[r,~] = size(Y);
        for i=1:N
           x1 = Y{i}.v1.p.x; 
           x2 = Y{i}.v2.p.x;
           y1 = Y{i}.v1.p.y;
           y2 = Y{i}.v2.p.y;
           
           plot(h,[x1 x2],[y1 y2],'b');
           %text(x1-0.05,y1,num2str(Y{i}.id),'FontSize',9,...
           %            'FontWeight','normal','Color','black','Parent',h);
           %scatter(h,x1,y1,'filled','r');
           %scatter(h,x2,y2,'filled','g');
                   

            %l=line([x x2],[y y+0.1],'Color',[1 0 0],'Parent',h);                   
            %uistack(l,'bottom') 
        end
        
    end
    
    if strcmp(mode,'marker')
        %hold(h,'on');
        y = ylim;
        plot(h,Y,y,'g');
    end

    if strcmp(mode,'marker_cross')
        %hold(h,'on');
        %y = ylim;
        %plot(h,Y(1,1),Y(1,2),'r','PointSize',10);
        x = Y(1,1);
        y = Y(1,2);
        patch([x-0.1,x+0.1,x+0.1,x-0.1],[y+0.1,y+0.1,y-0.1,y-0.1],ones(1,4),'FaceColor','r','Parent', h);
    end

    
    if strcmp(mode,'events')
        if ~isa(Y,'Events')
            disp('plotdata: input format is not events') 
            return
        end
        
        cla(h);
        Y.plot(h);
    end
    
    if strcmp(mode,'status')
        if ~isa(Y,'Status')
            disp('plotdata: input format is not status') 
            return
        end
        
        cla(h);
        Y.plot(h);
    end

end

