function res = lt(obj, val)
%% <  Tree element-wise less-than comparison, with scalar expansion
    x = max (min (obj.p.x, obj.q.x), min (val.p.x, val.q.x));
    
    if single(obj.get_y(x)) < single(val.get_y(x))
        res = 1;
    else
        res = 0;
    end
end
