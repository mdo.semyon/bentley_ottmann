classdef edge < handle
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        id
        v1
        v2
        p
        q
    end
   
    methods
        function e=edge(id,v1,v2)
                e.id = id;
                [a,b] = edge.swap(v1,v2);
                e.v1 = a;
                e.v2 = b;
        end        
        
        function d = get_d(e)
            d = sqrt((e.p.x - e.q.x)^2 + (e.p.y - e.q.y)^2);
        end
        
        function plot(e,h)
           %e.v1.plot(h);
           %e.v2.plot(h);
           
           x = (e.p.x + e.q.x)/2;
           y = (e.p.y + e.q.y)/2;
           %d = e.get_d();
           
           %text(x-0.005,y,num2str(d),'FontSize',6,'FontWeight','normal','Color',[1 1 1],'Parent',h);
           try
               l=line([e.p.x e.q.x],[e.p.y e.q.y],'Color',[1 0 0],'Parent',h);
               uistack(l,'bottom') 
           catch
               return
           end
           
        end
        
        function e2 = split(e,id,v)
            %��� ��� � ������������ �� ������� swap(v1,v2) ��� �����������
            %��� ������� v1 ����� �������������� v2. ������� ��� ����������
            %����� ������� ����� ������ ���������� �� ������ ���������, �
            %����� � ������. ��� ������ ��������� ���������� ������ �����
            %����������� v ��������� �� ������� ������ � ����� ������. �
            %������� ����� �������� � �������, ��� ��� � ������ ����.
            e2 = edge(id,e.v1,v);
            e.v1 = v;
        end
        
        %segment methods
        function y = get_y(e,x)

            % ���������, ����� �� ������� ����� ��������
             if max (e.p.x, x) > min (e.q.x, x)
                 y = NaN;
                 return
             end
            
             % ������ ���� ������� ������������
            if abs(e.p.x - e.q.x) == 0
                y = e.p.y;
                return
            end
       
            y = e.p.y + (e.q.y - e.p.y) *...
                (x - e.p.x) / (e.q.x - e.p.x);            
        end
        
        %������� � �������������� ������� �������
        function [left,right] = intersect(e,s)
            right = [];
            
            if ~isa(s,'edge')
                error('edge::intersect input paramenter should be of type segment');
            end

            a = e.p;
            b = e.q;
            c = s.p;
            d = s.q;
            
            %���� ������� ����� ����� ������� �� �������, ��� ��� ��
            %������������
            if e.p == s.p || e.q == s.q || e.p == s.q || e.q == s.p 
                left = [];
                return
            end
            
            if ~edge.intersect_1d(a.x, b.x, c.x, d.x) || ~edge.intersect_1d(a.y, b.y, c.y, d.y)
                left = [];
                return
            end
            
            m = bentley_ottmann_line(a, b);
            n = bentley_ottmann_line(c, d);
            zn = edge.det(m.a, m.b, n.a, n.b);
            
            if zn == 0 
                left = [];
                right = [];
            else
                left = point(-edge.det(m.c, m.b, n.c, n.b) / zn,...
                             -edge.det(m.a, m.c, n.a, n.c) / zn);
               
                if ~edge.betw(a.x, b.x, left.x) ||... 
                   ~edge.betw(a.y, b.y, left.y) ||...
                   ~edge.betw(c.x, d.x, left.x) ||...
                   ~edge.betw(c.y, d.y, left.y)
               
                    left = [];
                end
            end
        end        
    end

   methods (Static)
       %segment methods
        function D = det(a,b,c,d)

            if ~isa(a,'double') || ~isa(b,'double') || ~isa(c,'double') || ~isa(d,'double')
                error('edge::det input paramenters should be of type double');
            end
            
            D = a*d - b*c;
        end
        
        function res = intersect_1d(a,b,c,d)

            if ~isa(a,'double') || ~isa(b,'double') || ~isa(c,'double') || ~isa(d,'double')
                error('edge::intersect_1d input paramenters should be of type double');
            end
            
            if a > b
               [a,b] = edge.swap(a,b);
            end
            if c > d
                [c,d] = edge.swap(c,d);
            end
            res = max(a,c) <= min(b,d);
        end
        
        function res = betw(l,r,x)
            res = min(l,r) <= x && x <= max(l,r);
        end
        
        function [newa,newb] = swap(a,b)
           if isa(a,'vertex') && isa(b,'vertex')
               if a.p.x > b.p.x
                   newa = b;
                   newb = a;
               else
                   newa = a;
                   newb = b;                   
               end
           elseif isa(a,'point') && isa(b,'point')
              if a.x > b.x
                newa = b;
                newb = a;
               else
                   newa = a;
                   newb = b;                   
              end
           elseif isa(a,'double') && isa(b,'double')
               if a > b
                   newa = b;
                   newb = a;
               else
                   newa = a;
                   newb = b;                   
               end
           end
        end
    end
    
    methods
        %������� - id
        function set.id(e,index)
            e.id=index;
        end
        
        function index=get.id(e)
            index=e.id;
        end
        
        %������� - v1
        function set.v1(e,v1)
            e.v1=v1;
        end
        
        function v=get.v1(e)
            v=e.v1;
        end
        
        %������� - v2
        function set.v2(e,v2)
            e.v2 = v2;
        end
        
        function v=get.v2(e)
            v=e.v2;
        end        

        %p, q
        function p=get.p(e)
            p=e.v1.p;
        end
        
        function q=get.q(e)
            q=e.v2.p;
        end        
        
    end
end

