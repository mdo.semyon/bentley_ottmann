classdef Events<handle
    %Preparata. �������������� ���������. ��� 346.
    %�������������, ������������, ��� ��� ������ ��� ���� ������������ ��������� ������ 
    %E, ����������� ��� ������ �� ������� ����� �������, ������ ������������ ��������� �������� 
    %(� E �������� ������ ������������ ����� �������): 
    %a. MIN(E). ���������� ���������� ������� � E � ������� ���. 
    %b. ��������(�,E). �������� �������� � � ������ ������������, �������� � E. 
    %� ���������� � ���� ��������� ��������� ����������, ����� E ������������ ����� ��������: 
    %c. ����(�,E). ������, �������� �� �������� � ������ E. 
    properties
        NiL
        root
    end
    
    methods
        function rbTree=Events(p,t)
            import Nodes.*
            rbTree.root=ColoredEventNode(p,t);
               
            % root node is always black!!!
            rbTree.root.color=Colors.black;
               
            % NiL node is always black!!!
            rbTree.NiL=ColoredEventNode(NaN,NaN);
            rbTree.NiL.color=Colors.black;
               
            rbTree.NiL.left=rbTree.root;
            rbTree.NiL.right=rbTree.root;
            rbTree.root.parent=rbTree.NiL;
            rbTree.root.left=rbTree.NiL;
            rbTree.root.right=rbTree.NiL;
        end
        
        function node = addEvent(rbTree,p,t)
            import Nodes.*
            
            node=ColoredEventNode(p,t);
            rbTree.addNode(node);
        end
        
        function addNode(rbTree,node)
            import Nodes.*
            y=rbTree.NiL;
            x=rbTree.root;
            while x~=rbTree.NiL
                %if node.s1.id >= 8321
                %    disp(['node id : ',num2str(x.s1.id)]);
                %end    
               y=x;
               if node.p.x < x.p.x
                  x=x.left;
               elseif node.p.x > x.p.x
                   x=x.right;
               else
                   %return 
                   if node.p.y < x.p.y
                       x = x.left;
                   elseif node.p.y > x.p.y
                       x = x.right;
                   else
                       %��. events_corresponded_to_common_vertices.png
                       %����� ����� ����� ��������� �����
                       if node.type == 1 && x.type == 1
                           
                           if node.s1.q.y < x.s1.q.y
                               x = x.left;
                           elseif node.s1.q.y > x.s1.q.y
                               x = x.right;
                           end
                       
                       %����� ����� ����� �������� �����                           
                       elseif node.type == -1 && x.type == -1
                           
                           if node.s1.p.y < x.s1.p.y
                               x = x.left;
                           elseif node.s1.p.y > x.s1.p.y
                               x = x.right;
                           end                           
                               
                       %������ 1 ����� ��������� � ������ 2 �����
                       elseif node.type == 1 && x.type == -1                               
                       
                            x = x.right;
                           
                       %������ 2 ����� ��������� � ������ 1 �����                           
                       elseif node.type == -1 && x.type == 1                                
                           
                            x = x.left;

                       else
                           disp('Events : ����� � ����� ������. �� ��������������� ������.');
                           return
                       end
                   end
               end
            end
            
            node.parent=y;
            if y==rbTree.NiL
                rbTree.root=node;
            else
                if node.p.x < y.p.x
                    y.left=node;
                elseif node.p.x > y.p.x
                    y.right=node;
                
                else
                    %return
                    if node.p.y < y.p.y
                       y.left = node;
                   elseif node.p.y > y.p.y
                       y.right = node;
                    else
                       %��. events_corresponded_to_common_vertices.png
                       %����� ����� ����� ��������� �����
                       if node.type == 1 && y.type == 1
                           
                           if node.s1.q.y < y.s1.q.y
                               y.left = node;
                           elseif node.s1.q.y > y.s1.q.y
                               y.right = node;
                           end                           
                       
                       %����� ����� ����� �������� �����                           
                       elseif node.type == -1 && y.type == -1
                           
                           if node.s1.p.y < y.s1.p.y
                               y.left = node;
                           elseif node.s1.p.y > y.s1.p.y
                               y.right = node;
                           end                                              
                           
                       %������ 1 ����� ��������� � ������ 2 �����
                       elseif node.type == 1 && y.type == -1                               
                       
                           y.right = node;
                           
                       %������ 2 ����� ��������� � ������ 1 �����                           
                       elseif node.type == -1 && y.type == 1                                
                           
                           y.left = node;

                       else
                           disp('Events : ����� � ����� ������. �� ��������������� ������.');
                           return
                       end
                   end
                end
            end
            
            %insert node into tree
            node.left=rbTree.NiL;
            node.right=rbTree.NiL;
            node.color=Colors.red;
            
            %call the fixup method for setting the tree right
            rbInsertFixup(rbTree,node);
        end
        
        function y=rbDeleteNode(rbTree,node)
            import Nodes.*
            if node.left==rbTree.NiL || node.right==rbTree.NiL
                y=node;
            else
                y=treeSuccessor(rbTree,node);
                
            end
            
            if y.left==rbTree.NiL
                x=y.left;
            else
                x=y.right;
            end
            
            x.parent=y.parent;
            
            if y.parent==rbTree.NiL
                rbTree.root=x;
            else
                if y==y.parent.left
                    y.parent.left=x;
                else
                    y.parent.right=x;
                end
            end
            
            if y==node
                node.p.x=y.p.x;
            end
            
            if y.color==Colors.black
                rbDeleteFixup(rbTree,x);
            end
            
                    
        end

        %�������� �������� ���� �� �������
        function rbDeleteNode2(rbTree,z)
            import Nodes.*
            
            y = z;
            y_original_color = y.color;
            if z.left==rbTree.NiL
                x=z.right;
                rbTransplant(rbTree,z,z.right);
            elseif z.right == rbTree.NiL
                x = z.left;
                rbTransplant(rbTree,z,z.left);
            else
                y = treeMinimum(rbTree,z.right);
                y_original_color = y.color;
                x = y.right;
                
                if y.parent == z
                    x.parent = y;
                else
                    rbTransplant(rbTree,y,y.right);
                    y.right = z.right;
                    y.right.parent = y;
                end
                %--------
                rbTransplant(rbTree,z,y);
                y.left = z.left;
                y.left.parent = y;
                y.color = z.color;
                %--------
            end
            
            if y_original_color == Colors.black
               rbDeleteFixup(rbTree,x); 
            end
        end
        
        % method for searching for k- key in the tree.
        function node=treeSearch(rbTree,node,k)

            while node~=rbTree.NiL && k~=node.p.x
                if k<node.p.x
                    node=node.left;
                else
                    node=node.right;
                end
            end
            
            if isempty(node.p) || ~isa(node.p,'point')
                node = [];
            end
        end
    
        function plot(rbTree,h)
                        
            %f=figure;
            %axes('Parent',f,'XLim',[0 1],'YLim',[0 1],'XTick',[],'YTick',[]);
            %axes(h);
            %grid on;
            ploting(rbTree,h,rbTree.root,0,1,0.9);
        end
        
        
        function plot_(rbTree)
                        
            f=figure;
            axes('Parent',f,'XLim',[0 1],'YLim',[0 1],'XTick',[],'YTick',[]);
            %axes(h);
            grid on;
            ploting(rbTree,gca,rbTree.root,0,1,0.9);
        end
        
        function num=maxInTree(rbTree,node)
            while node.right~=rbTree.NiL
                node=node.right;
            end
            num=node.p.x;
        end
        
        function num=minInTree(rbTree,node)
            while node.left~=rbTree.NiL
                node=node.left;
            end
            num=node.p.x;
        end
        
        % minimum in tree helper function
        function min=treeMinimum(rbTree,node)
            while node.left~=rbTree.NiL
                node=node.left;
            end
            min=node;
        end        
        
        function num=numberOfNodes(rbTree,node)
            num=countNodes(rbTree,node,0);
        end
    end
    methods (Access=protected)
        
        % insertaion helper function
        function rbInsertFixup(rbTree,node)
            import Nodes.*
            while node.parent.color==Colors.red
                if node.parent==node.parent.parent.left
                    y=node.parent.parent.right;
                    if y.color==Colors.red
                        node.parent.color=Colors.black;
                        y.color=Colors.black;
                        node.parent.parent.color=Colors.red;
                        node=node.parent.parent;
                    else
                        if node==node.parent.right
                            node=node.parent;
                            leftRotate(rbTree,node);
                        end
                        node.parent.color=Colors.black;
                        node.parent.parent.color=Colors.red;
                        rightRotate(rbTree,node.parent.parent);
                    end
                else
                    y=node.parent.parent.left;
                    if y.color==Colors.red
                        node.parent.color=Colors.black;
                        y.color=Colors.black;
                        node.parent.parent.color=Colors.red;
                        node=node.parent.parent;
                    else
                        if node==node.parent.left
                            node=node.parent;
                            rightRotate(rbTree,node);
                        end
                        node.parent.color=Colors.black;
                        node.parent.parent.color=Colors.red;
                        leftRotate(rbTree,node.parent.parent);
                    end
                            
                end
            end
            rbTree.root.color=Colors.black;
        end
        
        % rotate left helper fuction
        function leftRotate(rbTree,node)
            y=node.right;
            node.right=y.left;
            if y.left~=rbTree.NiL
                y.left.parent=node;
            end
            y.parent=node.parent;
            if node.parent==rbTree.NiL
                rbTree.root=y;
            else
                if node==node.parent.left
                    node.parent.left=y;
                else
                    node.parent.right=y;
                end
                    
            end
            y.left=node;
            node.parent=y;
        end
        
        % rotate right helper function
        function rightRotate(rbTree,node)
            y=node.left;
            node.left=y.right;
            if y.right~=rbTree.NiL
                y.right.parent=node;
            end
            y.parent=node.parent;
            if node.parent==rbTree.NiL
                rbTree.root=y;
            else
                if node==node.parent.right
                    node.parent.right=y;
                else
                    node.parent.left=y;
                end
                    
            end
            y.right=node;
            node.parent=y;
        end
        
        % deletion helper function
        function rbDeleteFixup(rbTree,node)
            import Nodes.*
            while node~=rbTree.root && node.color==Colors.black
                if node==node.parent.left
                    w=node.parent.right;
                    if w.color==Colors.red
                        w.color=Colors.black;
                        node.parent.color=Colors.red;
                        leftRotate(rbTree,node.parent);
                        w=node.parent.right;
                    end
                    
                    if w.left.color==Colors.black && w.right.color==Colors.black
                        w.color=Colors.red;
                        node=node.parent;
                    else
                        if w.right.color==Colors.black
                            w.left.color=Colors.black;
                            w.color=Colors.red;
                            rightRotate(rbTree,w);
                            w=node.parent.right;
                        end
                        w.color=node.parent.color;
                        node.parent.color=Colors.black;
                        w.right.color=Colors.black;
                        leftRotate(rbTree,node.parent);
                        node=rbTree.root;
                    end
                else
                    w=node.parent.left;
                    if w.color==Colors.red
                        w.color=Colors.black;
                        node.parent.color=Colors.red;
                        rightRotate(rbTree,node.parent);
                        w=node.parent.left;
                    end
                    
                    if w.right.color==Colors.black && w.left.color==Colors.black
                        w.color=Colors.red;
                        node=node.parent;
                    else
                        if w.left.color==Colors.black
                            w.rightt.color=Colors.black;
                            w.color=Colors.red;
                            leftRotate(rbTree,w);
                            w=node.parent.left;
                        end
                        w.color=node.parent.color;
                        node.parent.color=Colors.black;
                        w.left.color=Colors.black;
                        rightRotate(rbTree,node.parent);
                        node=rbTree.root;
                    end
                end
            end
            node.color=Colors.black;
        end
        % successor find helper method
        function successor=treeSuccessor(rbTree,node)
            if node.right~=rbTree.NiL
                successor=treeMinimum(rbTree,node.right);
                return
            end
            y=node.parent;
            while y~=rbTree.NiL && node==y.right
                node=y;
                y=y.parent;
            end
            successor=y;
        end
        
        %�������� ��������������(������) ���� �� �������
        function rbTransplant(rbTree,u,v)
            if u.parent == rbTree.NiL
                rbTree.root = v;
            elseif u == u.parent.left
                u.parent.left = v;
            else
                u.parent.right = v;
            end
            
            v.parent = u.parent;            
        end
        
        function ploting(rbTree,h,node,x1,x2,y)
            import Nodes.*
            if ~isempty(node)&& node~=rbTree.NiL
               x=mean([x1 x2]);
               ploting(rbTree,h,node.left,x1,x,y-0.1);
               ploting(rbTree,h,node.right,x,x2,y-0.1);
               
               if node.color==Colors.red
                   color=[1 0 0];
               else
                   color=[0 0 0];
               end
               patch([x-0.015,x+0.045,x+0.045,x-0.015],[y+0.015,y+0.015,y-0.015,y-0.015],...
                   ones(1,4),'FaceColor',color,'Parent',h);
               
                if node.type == 1
                   text(x-0.005,y,[num2str(node.p.x),',',num2str(node.p.y),' - ',num2str(node.s1.q.x),',',num2str(node.s1.q.y)],'FontSize',6,...
                       'FontWeight','normal','Color','g','Parent',h);                   
                else
                   text(x-0.005,y,[num2str(node.p.x),',',num2str(node.p.y),' - ',num2str(node.s1.p.x),',',num2str(node.s1.p.y)],'FontSize',6,...
                       'FontWeight','normal','Color','g','Parent',h);                                       
                end

               %if node.type == 0
               %    text(x-0.005,y,num2str(node.p.x),'FontSize',6,...
               %        'FontWeight','normal','Color','g','Parent',h);                   
               %else
               %    text(x-0.005,y,[num2str(node.s1.id) ' : ' num2str(node.p.x)],'FontSize',6,...
               %        'FontWeight','normal','Color',[1 1 1],'Parent',h);
               %end
               
               try
                   if node==node.parent.left
                       l=line([x x2],[y y+0.1],'Color',[1 0 0],'Parent',h);
                   end
                   if node==node.parent.right
                       l=line([x x1],[y y+0.1],'Color',[0 0 1],'Parent',h);
                   end
                   uistack(l,'bottom') 
               catch
                   return
               end
           end
        end
       
        function numOfNodes=countNodes(rbTree,node,numOfNodes)
             if node~=rbTree.NiL
               numOfNodes=numOfNodes+1;
               numOfNodes=countNodes(rbTree,node.left,numOfNodes);
               numOfNodes=countNodes(rbTree,node.right,numOfNodes);
             end
        end
    end
    
    % get/set methods
    methods
        % get root method
        function root=get.root(rbTree)
           root=rbTree.root; 
        end
        % set root method
        function set.root(rbTree,root)
            rbTree.root=root;
        end
        %getNiL method
        function nil=get.NiL(rbTree)
            nil=rbTree.NiL;
        end
        function set.NiL(rbTree,nil)
            rbTree.NiL=nil;
        end
    end
    
end

