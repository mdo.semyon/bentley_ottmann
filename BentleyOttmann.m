    function [sE] = BentleyOttmann(E,V)

    sE = [];
    
    A = [];
    theStatus = [];
    [numberOfVertices,~] = size(V);
    [numberOfEdges,~] = size(E);
    
    % ���������� ������� �� ��������
    events = Events(E{1}.p,1);
    events.root.s1 = E{1};
    e = events.addEvent(E{1}.q,-1);
    e.s1 = E{1};

    for i = 1:numberOfEdges
        e1 = events.addEvent(E{i}.p,1);
        e1.s1 = E{i};
        e2 = events.addEvent(E{i}.q,-1);
        e2.s1 = E{i};
    end

    fig = figure;
    axes_edges = axes;
    plotdata(axes_edges,E,'edges',numberOfEdges);

    % �������� ���������� ����������� � �������������� ���������� ������
    while events.numberOfNodes(events.root)~= 0
        e = events.treeMinimum(events.root);
        events.rbDeleteNode2(e);

        if e.type == 1
            % e.x - ����� �����
            % s - ������� ������ �������� ������ e.x
            %[s,~] = get('edges',e);
            s = e.s1;
            plotdata(axes_edges,[e.p.x e.p.x],'marker');

            if isempty(theStatus)
                theStatus = Status(s);
                node_s = theStatus.root;
            else
                node_s = theStatus.addSegment(s);
            end

            node_s1 = theStatus.Under(node_s);
            if isa(node_s1,'Nodes.ColoredStatusNode')
                [cross_point,~] = node_s1.s.intersect(s);
                if ~isempty(cross_point) && single(cross_point.x) > single(e.p.x)
                    disp(['cross_point : (' num2str(cross_point.x) ',' num2str(cross_point.y) ')']);
                    a.s1 = node_s1.s;
                    a.s2 = s;
                    a.p = cross_point;
                    A = [A;a];
                end            
            end

            node_s2 = theStatus.Below(node_s);
            if isa(node_s2,'Nodes.ColoredStatusNode')
                [cross_point,~] = node_s2.s.intersect(s);
                if ~isempty(cross_point) && single(cross_point.x) > single(e.p.x)
                    disp(['cross_point : (' num2str(cross_point.x) ',' num2str(cross_point.y) ')']);
                    a.s1 = node_s2.s;
                    a.s2 = s;
                    a.p = cross_point;
                    A = [A;a];
                end
            end

        elseif e.type == -1
            % e.x - ������ �����
            % s - ������� ������ �������� ������ e.x
            %[s,~] = get('edges',e);
            s = e.s1;
            plotdata(axes_edges,[e.p.x e.p.x],'marker');

            node_s = theStatus.treeSearch(theStatus.root,s,e.p.x);
            if ~isa(node_s,'Nodes.ColoredStatusNode')
                disp('BentleyOttmann::unable to find status node')
                continue
            end

            node_s1 = theStatus.Under(node_s);
            node_s2 = theStatus.Below(node_s);
            if isa(node_s1,'Nodes.ColoredStatusNode') && isa(node_s2,'Nodes.ColoredStatusNode')
                [cross_point,~] = node_s1.s.intersect(node_s2.s);
                if ~isempty(cross_point) && single(cross_point.x) > single(e.p.x)
                    disp(['cross_point : (' num2str(cross_point.x) ',' num2str(cross_point.y) ')']);
                    a.s1 = node_s1.s;
                    a.s2 = node_s2.s;
                    a.p = cross_point;
                    A = [A;a];
                end
            end

            theStatus.rbDeleteNode2(node_s);
        elseif e.type == 0
            % e.x - ����� �����������
            % s1,s2 ������� �������������� � e.x (������ s1 = ���(s2) ����� �� e.x)
            %[s1,s2] = get('segmets',e);

            s1 = e.s1;
            s2 = e.s2;
            % s1,s2 ������� �������������� � e.x (������ s1 = ���(s2) ����� �� e.x)
            if s1 < s2
                s1 = e.s2;
                s2 = e.s1;
            end
            plotdata(axes_edges,[e.p.x e.p.y],'marker_cross');
            node_s1 = theStatus.treeSearch(theStatus.root,s1,e.p.x);
            if ~isa(node_s1,'Nodes.ColoredStatusNode') 
                disp('BentleyOttmann::unable to find status node')
                continue
            end

            node_s2 = theStatus.treeSearch(theStatus.root,s2,e.p.x);
            if ~isa(node_s2,'Nodes.ColoredStatusNode')
                disp('BentleyOttmann::unable to find status node')
                continue
            end

            node_s3 = theStatus.Under(node_s1);
            if isa(node_s3,'Nodes.ColoredStatusNode')
                [cross_point,~] = node_s3.s.intersect(s2);
                if ~isempty(cross_point) && single(cross_point.x) > single(e.p.x)
                    disp(['cross_point : (' num2str(cross_point.x) ',' num2str(cross_point.y) ')']);
                    a.s1 = node_s3.s;
                    a.s2 = s2;
                    a.p = cross_point;
                    A = [A;a];
                end
            end

            node_s4 = theStatus.Below(node_s2);
            if isa(node_s4,'Nodes.ColoredStatusNode')
                [cross_point,~] = s1.intersect(node_s4.s);
                if ~isempty(cross_point) && single(cross_point.x) > single(e.p.x)
                    disp(['cross_point : (' num2str(cross_point.x) ',' num2str(cross_point.y) ')']);
                    a.s1 = s1;
                    a.s2 = node_s4.s;
                    a.p = cross_point;
                    A = [A;a];
                end
            end

            theStatus.swap(node_s1,node_s2);
            
            %split edges in cross_point
            sE = [sE;[node_s1.s.id node_s2.s.id e.p.x e.p.y]];
            %{
            [numberOfVertices,~] = size(V);
            [numberOfEdges,~] = size(E);
            
            v = vertex(numberOfVertices + 1,e.p);
            a2 = node_s1.s.split(numberOfEdges + 1,v);
            b2 = node_s2.s.split(numberOfEdges + 2,v);
            
            V{numberOfVertices + 1} = v;
            E{numberOfEdges + 1} = a2;
            E{numberOfEdges + 2} = b2;
            %}
        end

        %�� ������ ��������, ������������� ��������� A � �������� �����
        %����������� � �������.
        r = size(A,1);
        for i = 1:r
            a = A(i);

            e = events.treeSearch(events.root,a.p.x);
            if isempty(e)
                e = events.addEvent(a.p,0);
                e.s1 = a.s1;
                e.s2 = a.s2;
            end
        end
        A = [];
    end
end

