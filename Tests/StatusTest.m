%������������� ������ ��������� ��� ������ � ���������
%1) ��������� ��� ������� � ���������
%2) �� ������ ���� �������� ����������� ������� ������, �������� ��� �
%��������� � ������� �� ���������
%3) ��������� � ��������� ����� ����������� � ��������� � ���������������
%����� (x1<x2<....<xn), ���� � ��������� ��� ���� ������� � �����-�� x,
%���������� �� y.
clear all
import Nodes.*
%mode: 0 - predefined, 1 - random
mode = 0;

s = cell(10,1);

if mode == 0
    
    % ���������� ������� �� ��������

    %\   /
    % \ /
    %  o
p = point(0,0);
q1 = point(-1,1);
q2 = point(1,1);

    v0 = vertex(10,p);
    v1 = vertex(11,q1);
    v2 = vertex(12,q2);
    s{1,:} = edge(1,v1,v0);
    s{2,:} = edge(2,v0,v2);
    
    theStatus = Status(s{1,:});
    theStatus.addSegment(s{2,:});

    
    %  o
    % / \
    %/   \
p = point(0,3);
q1 = point(-1,2);
q2 = point(1,2);

    v0 = vertex(20,p);
    v1 = vertex(21,q1);
    v2 = vertex(22,q2);
    s{3,:} = edge(3,v1,v0);
    s{4,:} = edge(4,v0,v2);
    theStatus.addSegment(s{3,:});
    theStatus.addSegment(s{4,:});
    
    %\
    % \
    %  o
    %   \
    %    \
p = point(0,5);
q1 = point(-1,6);
q2 = point(1,4);

    v0 = vertex(30,p);
    v1 = vertex(31,q1);
    v2 = vertex(32,q2);
    s{5,:} = edge(5,v1,v0);
    s{6,:} = edge(6,v0,v2);
    theStatus.addSegment(s{5,:});
    theStatus.addSegment(s{6,:});

    %\
    % \ 
    %  o
    % /
    %/ 
p = point(0,11);
q1 = point(-1,10);
q2 = point(-1,12);

    v0 = vertex(50,p);
    v1 = vertex(51,q1);
    v2 = vertex(52,q2);
    s{7,:} = edge(9,v1,v0);
    s{8,:} = edge(10,v0,v2);
    theStatus.addSegment(s{7,:});
    theStatus.addSegment(s{8,:});        
    
    %  /
    % / 
    %o
    % \
    %  \
p = point(0,14);
q1 = point(1,13);
q2 = point(1,15);

    v0 = vertex(40,p);
    v1 = vertex(41,q1);
    v2 = vertex(42,q2);
    s{9,:} = edge(7,v1,v0);
    s{10,:} = edge(8,v0,v2);
    theStatus.addSegment(s{9,:});
    theStatus.addSegment(s{10,:});    
    
    
elseif mode == 1
    % ���������� ������� �� ��������
    p = point(5*randn,5*randn);
    q = point(5*randn,5*randn);
    v1 = vertex(1,p);
    v2 = vertex(2,q);
    s = edge(1,v1,v2);
    
    theStatus = Status(s);

    for i = 1:10
        p = point(5*randn,5*randn);
        q = point(5*randn,5*randn);
        v1 = vertex(i + 2,p);
        v2 = vertex(10 + i + 2,q);
        s = edge(i + 1,v1,v2);

        theStatus.addSegment(s);
    end
end

f=figure;
h = axes('Parent',f,'XLim',[0 1],'YLim',[0 1],'XTick',[],'YTick',[]);
plot(theStatus,h);


node_s = theStatus.treeSearch(theStatus.root,s{1,:},0);
theStatus.rbDeleteNode2(node_s);
cla(h);
plot(theStatus,h);

node_s = theStatus.treeSearch(theStatus.root,s{2,:},0);
theStatus.rbDeleteNode2(node_s);
cla(h);
plot(theStatus,h);

node_s = theStatus.treeSearch(theStatus.root,s{3,:},0);
theStatus.rbDeleteNode2(node_s);
cla(h);
plot(theStatus,h);


node_s = theStatus.treeSearch(theStatus.root,s{4,:},0);
theStatus.rbDeleteNode2(node_s);
cla(h);
plot(theStatus,h);


node_s = theStatus.treeSearch(theStatus.root,s{5,:},0);
theStatus.rbDeleteNode2(node_s);
cla(h);
plot(theStatus,h);

