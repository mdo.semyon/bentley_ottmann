
clear all
import Nodes.*
    
load('objC.mat');
load('objE.mat');

%[numberOfEdges,~] = size(objE);
theStatus = [];
numberOfEdges = 430;

fig = figure;
axes_edges = gca;
plotdata(axes_edges,objE,'edges',numberOfEdges);




events = Events(objE{1}.p,1);
events.root.s1 = objE{1};

e=ColoredEventNode(objE{1}.q,-1);
e.s1 = objE{1};
events.addNode(e);        


for i = 2:numberOfEdges

    if i == 4430 || i == 8322 || i == 10306 || i == 15196 || i == 17268
       continue;
    end
    rebro = objE{i};


    e1=ColoredEventNode(rebro.p,1);
    e1.s1 = rebro;
    events.addNode(e1);        

    e2=ColoredEventNode(rebro.q,-1);
    e2.s1 = rebro;
    events.addNode(e2);        

    disp(num2str(rebro.id));
end

% �������� ���������� ����������� � �������������� ���������� ������
while events.numberOfNodes(events.root)~= 0
    e = events.treeMinimum(events.root);
    events.rbDeleteNode2(e);

    if e.type == 1
        % e.x - ����� �����
        % s - ������� ������ �������� ������ e.x
        %[s,~] = get('edges',e);
        s = e.s1;
        plotdata(axes_edges,[e.p.x e.p.x],'marker');

        if isempty(theStatus)
            theStatus = Status(s);
            node_s = theStatus.root;
        else
            node_s = theStatus.addSegment(s);
        end

    elseif e.type == -1
        % e.x - ������ �����
        % s - ������� ������ �������� ������ e.x
        %[s,~] = get('edges',e);
        s = e.s1;
        plotdata(axes_edges,[e.p.x e.p.x],'marker');

        node_s = theStatus.treeSearch(theStatus.root,s,e.p.x);
        if ~isa(node_s,'Nodes.ColoredStatusNode')
            disp('BentleyOttmann::unable to find status node')
            continue
        end

        theStatus.rbDeleteNode2(node_s);
    elseif e.type == 0
        % e.x - ����� �����������
        % s1,s2 ������� �������������� � e.x (������ s1 = ���(s2) ����� �� e.x)
        %[s1,s2] = get('segmets',e);

        s1 = e.s1;
        s2 = e.s2;
        % s1,s2 ������� �������������� � e.x (������ s1 = ���(s2) ����� �� e.x)
        if s1 < s2
            s1 = e.s2;
            s2 = e.s1;
        end
        plotdata(axes_edges,[e.p.x e.p.y],'marker_cross');
        node_s1 = theStatus.treeSearch(theStatus.root,s1,e.p.x);
        if ~isa(node_s1,'Nodes.ColoredStatusNode') 
            disp('BentleyOttmann::unable to find status node')
            continue
        end

        node_s2 = theStatus.treeSearch(theStatus.root,s2,e.p.x);
        if ~isa(node_s2,'Nodes.ColoredStatusNode')
            disp('BentleyOttmann::unable to find status node')
            continue
        end


        theStatus.swap(node_s1,node_s2);
    end
end