%������������� ������ ��������� ��� ������ � ���������
%1) ��������� ��� ������� � ���������
%2) �� ������ ���� �������� ����������� ������� ������, �������� ��� �
%��������� � ������� �� ���������
%3) ��������� � ��������� ����� ����������� � ��������� � ���������������
%����� (x1<x2<....<xn), ���� � ��������� ��� ���� ������� � �����-�� x,
%���������� �� y.
clear all
import Nodes.*
%mode: 0 - predefined, 1 - random, 2 - real data
mode = 2;


if mode == 0
    
    % ���������� ������� �� ��������
p = point(2,0);
q = point(3,1);
    v1 = vertex(11,p);
    v2 = vertex(12,q);
    rebro = edge(1,v1,v2);

    events = Events(p,1);
    events.root.s1 = rebro;
    
    
    e=ColoredEventNode(q,-1);
    e.s1 = rebro;
    events.addNode(e);        
   

p = point(3,1);
q = point(4,2);
    v1 = vertex(21,p);
    v2 = vertex(22,q);
    rebro = edge(2,v1,v2);

    e1=ColoredEventNode(p,1);
    e1.s1 = rebro;
    events.addNode(e1);        

    e2=ColoredEventNode(q,-1);
    e2.s1 = rebro;
    events.addNode(e2);        
           

p = point(1,1);
q = point(2,0);
    v1 = vertex(31,p);
    v2 = vertex(32,q);
    rebro = edge(3,v1,v2);

    e1=ColoredEventNode(p,1);
    e1.s1 = rebro;
    events.addNode(e1);        

    e2=ColoredEventNode(q,-1);
    e2.s1 = rebro;
    events.addNode(e2);        
    
p = point(1,1);
q = point(2,2);
    v1 = vertex(41,p);
    v2 = vertex(42,q);
    rebro = edge(4,v1,v2);

    e1=ColoredEventNode(p,1);
    e1.s1 = rebro;
    events.addNode(e1);        

    e2=ColoredEventNode(q,-1);
    e2.s1 = rebro;
    events.addNode(e2);        
    
p = point(2,2);
q = point(3,3);
    v1 = vertex(51,p);
    v2 = vertex(52,q);
    rebro = edge(5,v1,v2);

    e1=ColoredEventNode(p,1);
    e1.s1 = rebro;
    events.addNode(e1);        

    e2=ColoredEventNode(q,-1);
    e2.s1 = rebro;
    events.addNode(e2);        

elseif mode == 1
    % ���������� ������� �� ��������
    p = point(5*randn,5*randn);
    q = point(5*randn,5*randn);
    v1 = vertex(1,p);
    v2 = vertex(2,q);
    rebro = edge(1,v1,v2);

    events = Events(p,1);
    events.root.s1 = rebro;
    e = events.addEvent(q,-1);
    e.s1 = rebro;

    for i = 1:10
        p = point(5*randn,5*randn);
        q = point(5*randn,5*randn);
        v1 = vertex(i + 2,p);
        v2 = vertex(10 + i + 2,q);
        rebro = edge(i + 1,v1,v2);

        e1 = events.addEvent(p,1);
        e1.s1 = rebro;
        e2 = events.addEvent(q,-1);
        e2.s1 = rebro;
    end
    
elseif mode == 2
    load('objC.mat');
    load('objE.mat');
    
    [numberOfEdges,~] = size(objE);
    
    
    events = Events(objE{1}.p,1);
    events.root.s1 = objE{1};
    
    e=ColoredEventNode(objE{1}.q,-1);
    e.s1 = objE{1};
    events.addNode(e);        

    
    %{
    test = true(numberOfEdges,1);
    
    for i = 1:numberOfEdges
        for j = 1:numberOfEdges
            if objE{i}.p == objE{j}.p && objE{i}.q == objE{j}.q && objE{i}.id ~= objE{j}.id
                if test(objE{i}.id) && test(objE{j}.id)
                    test(objE{i}.id) = false;
                end
            end
            disp([num2str(i),':',num2str(j)]);
        end
    end
    
    
    %}
    
    
    for i = 2:numberOfEdges
        %if ~test(i)
        %   continue; 
        %end
        
        if i == 4430 || i == 8322 || i == 10306 || i == 15196 || i == 17268
           continue;
        end
        rebro = objE{i};
        
        
        e1=ColoredEventNode(rebro.p,1);
        e1.s1 = rebro;
        events.addNode(e1);        

        e2=ColoredEventNode(rebro.q,-1);
        e2.s1 = rebro;
        events.addNode(e2);        

        disp(num2str(rebro.id));
    end
    
end



f=figure;
h = axes('Parent',f,'XLim',[0 1],'YLim',[0 1],'XTick',[],'YTick',[]);
plot(events,h);

e = events.treeMinimum(events.root);
events.rbDeleteNode2(e);
cla(h);
plot(events,h);


e = events.treeMinimum(events.root);
events.rbDeleteNode(e);
cla(h);
plot(events,h);
