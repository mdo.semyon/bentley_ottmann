%clear all
import Nodes.*
    
%load('objC.mat');
%load('objE.mat');
objC = objVertices;
objE = objEdges;


[numberOfEdges,~] = size(objE);
%numberOfEdges = 430;
theStatus = [];
sE = [];
A = [];

%{
fig = figure;
axes_edges = gca;

disp('start plotting')
tic
plotdata(axes_edges,objE,'edges',numberOfEdges);
plot_time = toc;
disp(['plotting time = ', num2str(plot_time)]);
%}

events = Events(objE{1}.p,1);
events.root.s1 = objE{1};

e=ColoredEventNode(objE{1}.q,-1);
e.s1 = objE{1};
events.addNode(e);        


disp('start fill events')
tic
for i = 2:numberOfEdges

    if i == 22326 || i == 24220 || i == 24504 %|| i == 15196 || i == 17268
       continue;
    end
    rebro = objE{i};


    e1=ColoredEventNode(rebro.p,1);
    e1.s1 = rebro;
    events.addNode(e1);        

    e2=ColoredEventNode(rebro.q,-1);
    e2.s1 = rebro;
    events.addNode(e2);        

    disp(num2str(rebro.id));
end
events_time = toc;
disp(['events time = ', num2str(events_time)]);


%����� ������� ����� ����� �����������
%{
p = point(80,120);
q = point(100,100);

    v1 = vertex(1451,p);
    v2 = vertex(1452,q);
    rebro = edge(1000,v1,v2);

    e1=ColoredEventNode(p,1);
    e1.s1 = rebro;
    events.addNode(e1);        

    e2=ColoredEventNode(q,-1);
    e2.s1 = rebro;
    events.addNode(e2);        
    
    plot(axes_edges,[p.x q.x],[p.y q.y],'b');
%}
%progressbar('progress');

%clear all
%import Nodes.*
    
%theStatus = [];
%sE = [];
%A = [];
%load('events_Yaroslavl.mat');
numberOfEvents = events.numberOfNodes(events.root);
errorID = 0;
N = 1;

disp('start crossings')
% �������� ���������� ����������� � �������������� ���������� ������
while events.numberOfNodes(events.root)~= 0
    e = events.treeMinimum(events.root);
    Log('event','found minimum event',e);
    events.rbDeleteNode2(e);
    
    N = N + 1;

    %progressbar(N/numberOfEvents);
    
    if e.type == 1
        % e.x - ����� �����
        % s - ������� ������ �������� ������ e.x
        %[s,~] = get('edges',e);
        s = e.s1;
        %plotdata(axes_edges,[e.p.x e.p.x],'marker');

        Log('status','create status node (before)',theStatus);
        if isempty(theStatus)
            theStatus = Status(s);
            node_s = theStatus.root;
        else
            node_s = theStatus.addSegment(s);
        end
        Log('status','create status node (after)',theStatus,node_s);

        node_s1 = theStatus.Under(node_s);
        if isa(node_s1,'Nodes.ColoredStatusNode')
            [cross_point,~] = node_s1.s.intersect(s);
            if ~isempty(cross_point) && single(cross_point.x) > single(e.p.x)
                disp(['cross_point : (' num2str(cross_point.x) ',' num2str(cross_point.y) ')']);
                a.s1 = node_s1.s;
                a.s2 = s;
                a.p = cross_point;
                A = [A;a];
            end            
        else
            errorID = errorID + 1;
            saveErrorInfoForFurtherDebugging(errorID, 'Under', e, node_s, theStatus);            
        end
        

        node_s2 = theStatus.Below(node_s);
        if isa(node_s2,'Nodes.ColoredStatusNode')
            [cross_point,~] = node_s2.s.intersect(s);
            if ~isempty(cross_point) && single(cross_point.x) > single(e.p.x)
                disp(['cross_point : (' num2str(cross_point.x) ',' num2str(cross_point.y) ')']);
                a.s1 = node_s2.s;
                a.s2 = s;
                a.p = cross_point;
                A = [A;a];
            end
        else
            errorID = errorID + 1;
            saveErrorInfoForFurtherDebugging(errorID, 'Below', e, node_s, theStatus);            
        end
        

    elseif e.type == -1
        % e.x - ������ �����
        % s - ������� ������ �������� ������ e.x
        %[s,~] = get('edges',e);
        s = e.s1;
        %plotdata(axes_edges,[e.p.x e.p.x],'marker');

        node_s = theStatus.treeSearch(theStatus.root,s,e.p.x);
        if ~isa(node_s,'Nodes.ColoredStatusNode')
            errorID = errorID + 1;            
            saveErrorInfoForFurtherDebugging(errorID, 'treeSearch', e, s, theStatus);
            continue
        end

        node_s1 = theStatus.Under(node_s);
        node_s2 = theStatus.Below(node_s);
        if isa(node_s1,'Nodes.ColoredStatusNode') && isa(node_s2,'Nodes.ColoredStatusNode')
            [cross_point,~] = node_s1.s.intersect(node_s2.s);
            if ~isempty(cross_point) && single(cross_point.x) > single(e.p.x)
                disp(['cross_point : (' num2str(cross_point.x) ',' num2str(cross_point.y) ')']);
                a.s1 = node_s1.s;
                a.s2 = node_s2.s;
                a.p = cross_point;
                A = [A;a];
            end
        end
        
        if ~isa(node_s1,'Nodes.ColoredStatusNode')
            errorID = errorID + 1;            
            saveErrorInfoForFurtherDebugging(errorID, 'Under', e, node_s, theStatus);            
        end
        if ~isa(node_s2,'Nodes.ColoredStatusNode')
            errorID = errorID + 1;            
            saveErrorInfoForFurtherDebugging(errorID, 'Below', e, node_s, theStatus);            
        end

        Log('status','delete status node (before)',theStatus,node_s);
        theStatus.rbDeleteNode2(node_s);
        Log('status','delete status node (after)',theStatus,node_s);
    elseif e.type == 0
        % e.x - ����� �����������
        % s1,s2 ������� �������������� � e.x (������ s1 = ���(s2) ����� �� e.x)
        %[s1,s2] = get('segmets',e);

        s1 = e.s1;
        s2 = e.s2;
        % s1,s2 ������� �������������� � e.x (������ s1 = ���(s2) ����� �� e.x)
        if s1 < s2
            s1 = e.s2;
            s2 = e.s1;
        end
        %plotdata(axes_edges,[e.p.x e.p.y],'marker_cross');
        node_s1 = theStatus.treeSearch(theStatus.root,s1,e.p.x);
        if ~isa(node_s1,'Nodes.ColoredStatusNode') 
            errorID = errorID + 1;            
            saveErrorInfoForFurtherDebugging(errorID, 'treeSearch', e, s1, theStatus);
            continue
        end

        node_s2 = theStatus.treeSearch(theStatus.root,s2,e.p.x);
        if ~isa(node_s2,'Nodes.ColoredStatusNode')
            errorID = errorID + 1;            
            saveErrorInfoForFurtherDebugging(errorID, 'treeSearch', e, s2, theStatus);
            continue
        end

        node_s3 = theStatus.Under(node_s1);
        if isa(node_s3,'Nodes.ColoredStatusNode')
            [cross_point,~] = node_s3.s.intersect(s2);
            if ~isempty(cross_point) && single(cross_point.x) > single(e.p.x)
                disp(['cross_point : (' num2str(cross_point.x) ',' num2str(cross_point.y) ')']);
                a.s1 = node_s3.s;
                a.s2 = s2;
                a.p = cross_point;
                A = [A;a];
            end
        else
            errorID = errorID + 1;            
            saveErrorInfoForFurtherDebugging(errorID, 'Under', e, node_s1, theStatus);            
        end

        node_s4 = theStatus.Below(node_s2);
        if isa(node_s4,'Nodes.ColoredStatusNode')
            [cross_point,~] = s1.intersect(node_s4.s);
            if ~isempty(cross_point) && single(cross_point.x) > single(e.p.x)
                disp(['cross_point : (' num2str(cross_point.x) ',' num2str(cross_point.y) ')']);
                a.s1 = s1;
                a.s2 = node_s4.s;
                a.p = cross_point;
                A = [A;a];
            end
        else
            errorID = errorID + 1;            
            saveErrorInfoForFurtherDebugging(errorID, 'Below', e, node_s2, theStatus);            
        end

        theStatus.swap(node_s1,node_s2);

        %split edges in cross_point
        sE = [sE;[node_s1.s.id node_s2.s.id e.p.x e.p.y]];
    end

    %�� ������ ��������, ������������� ��������� A � �������� �����
    %����������� � �������.
    r = size(A,1);
    for i = 1:r
        a = A(i);

        e = events.treeSearch(events.root,a.p.x);
        if isempty(e)
            e = events.addEvent(a.p,0);
            e.s1 = a.s1;
            e.s2 = a.s2;
            Log('event','add new event',e);
        end
    end
    A = [];

    disp(['Proceeded ',num2str(N),' of ',num2str(numberOfEvents),' events.']);
end
