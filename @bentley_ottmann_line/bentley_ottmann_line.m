classdef bentley_ottmann_line<handle
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        a
        b
        c
    end
    
    methods
        function the_line=bentley_ottmann_line(p,q)
            the_line.a = p.y - q.y;
            the_line.b = q.x - p.x;
            the_line.c = -the_line.a * p.x - the_line.b * p.y;
            the_line.norm();
        end        
        
        function L = dist(the_line,p)
            L = the_line.a * p.x + the_line.b*p.y + the_line.c;
        end
    end
    
    methods (Access = private)
        function norm(the_line)
            z = sqrt(the_line.a*the_line.a + the_line.b*the_line.b);
            if abs(z) > 0
                the_line.a = the_line.a/z;
                the_line.b = the_line.b/z;
                the_line.c = the_line.c/z;
            end
        end
    end
    
    methods
        %����������� - a
        function set.a(the_line,a)
            the_line.a=a;
        end
        
        function a=get.a(the_line)
            a=the_line.a;
        end        
        
        %����������� - b
        function set.b(the_line,b)
            the_line.b=b;
        end
        
        function b=get.b(the_line)
            b=the_line.b;
        end
        
        %����������� - c
        function set.c(the_line,c)
            the_line.c=c;
        end
        
        function c=get.c(the_line)
            c=the_line.c;
        end        
    end
end

